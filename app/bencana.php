<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bencana extends Model
{
    protected $fillable = ['jenis_bencana','deskripsi_bencana','provinsi_bencana',
  'kabupaten_bencana', 'kecamatan','kode_pos','alamat_lengkap','foto_bencana','status','id_pelapor'];

  public function pelapor(){
    return $this->belongsTo('App\User','id_pelapor','id');
  }

}
